#ifndef UMATH_H
#define UMATH_H

#include <stdint.h>

extern uint32_t getmaxdelimiter(const uint32_t num);
extern uint32_t getmindelimiter(const uint32_t num);
extern void getrandom(void* buff, const uint32_t count);


#endif