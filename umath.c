#include "umath.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>


static uint32_t isPrimary(const uint32_t num) {
	uint32_t n = (uint32_t)sqrt(num);

	if (n == 1)
		return 0;

	for (uint32_t i = 2; i <= n; i++)
		if ((num % i) == 0)
			return 0;

	return 1;
}

uint32_t getmaxdelimiter(const uint32_t num) {
	uint32_t delimiter = 1;
	uint32_t c = num;

	while (delimiter <= c) {
		if (isPrimary(delimiter)) {
			if ((c % delimiter) == 0) {
				c /= delimiter;
				if (delimiter == c)
					break;
				continue;
			}
		}
		delimiter++;
	}

	return delimiter;
}

uint32_t getmindelimiter(const uint32_t num) {
	uint32_t delimiter = 1;
	uint32_t c = num;

	while (delimiter <= c) {
		if (isPrimary(delimiter)) {
			if ((c % delimiter) == 0) {
				return delimiter;
			}
		}
		delimiter++;
	}

	return delimiter;
}

void getrandom(void* buff, const uint32_t count) {
	char* o = (char*)malloc(sizeof(char) * count);
	srand(time(NULL));

	for (uint32_t i = 0; i < count; i++)
		o[i] = rand() % 95 + 33;

	memcpy(buff, o, sizeof(char) * count);
	free(o);
}
