The program for xoring files.
Build:
    Exec make in project's folder
Usage:
    xorer -h : print help
    xorer -i file -o xoredfile : create a xored file by a random generated key. The key size is a minimal delimiter of input file size.
    xorer -i file -o xoredfile -k key : xor file by a custom key
    xorer -i file -o xoredfile -b : create a xored file by a random generated key. The key size is a maximum delimiter of input file size (delimiter is not equal input file size).