#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <getopt.h>

#include "umath.h"

static int xorbuffer(void* destbuffer, const char* sourcebuffer, const uint32_t count, const char* key, const uint32_t keysize);
static void showhelp();

int main(int argc, char** argv) {
	char* inputfile = NULL;
	char* outputfile = NULL;
	char* key = NULL;
	int c = 0;
	int flag = 0;
	uint32_t (*getkeysize)(uint32_t);

	while ((c = getopt (argc, argv, "i:o:k:b")) != -1)
		switch (c) {
			case 'i':
				inputfile = optarg;
				break;
			case 'o':
				outputfile = optarg;
				break;
			case 'k':
				key = optarg;
				break;
			case 'b':
				flag = 1;
				break;
			default:
				showhelp();
				break;
		}

	if (flag)
		getkeysize = getmaxdelimiter;
	else
		getkeysize = getmindelimiter;

	if (!inputfile || !outputfile || strlen(inputfile) == 0 || strlen(outputfile) == 0) {
		showhelp();
		return 0;
	}

	FILE* ifile;
	FILE* ofile;

	ifile = fopen(inputfile, "rb");
	ofile = fopen(outputfile, "wb");

	if (ifile <= 0) {
		fprintf(stderr, "Can't open input file\n");
		return -1;
	}
	if (ofile <= 0) {
		fprintf(stderr, "Can't open onput file\n");
		return -1;
	}

	fseek(ifile, 0, SEEK_END);
	uint32_t size = ftell(ifile);
	char* buff = (char*)malloc(sizeof(char) * size);
	rewind(ifile);

	fread(buff, 1, size, ifile);
	uint32_t keysize = 0;
	//here need xoring output
	if (!key) {
		keysize = (*getkeysize)(size);
		fprintf(stderr, "Size of key: %i\nFile size: %i\n", keysize, size);
		key = (char*)malloc(sizeof(char)*keysize);
		getrandom(key, keysize);
		fprintf(stderr, "Key: %s\n", key);
	} else {
		keysize = strlen(key);
		if (keysize != getmindelimiter(size)) {
			fprintf(stderr, "%i != %i\n", keysize, (*getkeysize)(size));
			fclose(ifile);
			fclose(ofile);
			free(buff);
			return -1;
		}
	}
	xorbuffer(buff, buff, size, key, keysize);

	fwrite(buff, 1, size, ofile);

	fclose(ifile);
	fclose(ofile);
	free(buff);

	return 0;
}

int xorbuffer(void* destbuffer, const char* sourcebuffer, const uint32_t count, const char* key, const uint32_t keysize) {
	if (!destbuffer || !sourcebuffer || !key)
		return -1;

	char* o = (char*)malloc(sizeof(char) * count);
	memcpy(o, sourcebuffer, sizeof(char) * count);

	uint32_t j = 0;
	while (j < count) {
		for (uint32_t i = 0; i < keysize; i++)
			o[j + i] ^= key[i];
		j += keysize;
	}
	memcpy(destbuffer, o, sizeof(char) * count);

	free(o);
	return 0;
}

void showhelp() {
	fprintf(stderr, "Usage: <-i input filename>\n <-o output filename>\n [-k key] [-b big key]\n");
}
