TARGET = xorer
CFLAGS = 
all: xorer
xorer: main.c umath.c
	gcc $(CFLAGS) -std=c99 -Wall -o $(@) $(^) -o xorer -lm -static
clean:
	rm -rf $(TARGET) *.o

